

#include <Urho3D/Engine/Application.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Input/InputEvents.h>
#include <Urho3D/Urho3DAll.h>

using namespace Urho3D;
class App : public Application
{
public:
  App(Context* context) :
  Application(context)
  {
  }
  virtual void Setup()
  {
    // Called before engine initialization. engineParameters_ member variable can be modified here
    engineParameters_[EP_FULL_SCREEN] = false;
  }
  virtual void Start()
  {
    // Called after engine initialization. Setup application & subscribe to events here
    SubscribeToEvent(E_KEYDOWN, URHO3D_HANDLER(App, HandleKeyDown));
//    SubscribeToEvent(E_, Urho3D::EventHandler *handler)
    
    // Get default style
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    XMLFile* xmlFile = cache->GetResource<XMLFile>("UI/DefaultStyle.xml");
    
    // Create console
    Console* console = engine_->CreateConsole();
    console->SetDefaultStyle(xmlFile);
    console->GetBackground()->SetOpacity(0.8f);
    
    // Create debug HUD.
    DebugHud* debugHud = engine_->CreateDebugHud();
    debugHud->SetDefaultStyle(xmlFile);
    
    auto input = GetSubsystem<Input>();
    input->SetMouseMode(MM_FREE);
    input->SetMouseVisible(true);
    
    SetupUI();
    CreateButton("foo", IntVector2(50, 50));
  }
  void SetupUI()
  {
  
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    UI* ui = GetSubsystem<UI>();
    UIElement* root = ui->GetRoot();
    root->SetDefaultStyle(cache->GetResource<XMLFile>("UI/DefaultStyle.xml"));
    
    // Construct new Text object, set string to display and font to use
    Text* instructionText = ui->GetRoot()->CreateChild<Text>();
    instructionText->SetText("Drag on the buttons to move them around.\n"
                             "Touch input allows also multi-drag.\n"
                             "Press SPACE to show/hide tagged UI elements.");
    instructionText->SetFont(cache->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 15);
    instructionText->SetTextAlignment(HA_CENTER);

    // Position the text relative to the screen center
    instructionText->SetHorizontalAlignment(HA_CENTER);
    instructionText->SetVerticalAlignment(VA_CENTER);
    instructionText->SetPosition(0, ui->GetRoot()->GetHeight() / 4);
  }
  
  
  void CreateButton(const char* label, IntVector2 position)
  {
    Button* b = new Button(context_);
    GetSubsystem<UI>()->GetRoot()->AddChild(b);
    b->SetStyle("Button");
    b->SetSize(300, 100);
    b->SetPosition(position);
    
    Text* t = new Text(context_);
    b->AddChild(t);
    t->SetStyle("Text");
    t->SetHorizontalAlignment(HA_CENTER);
    t->SetVerticalAlignment(VA_CENTER);
    t->SetText(label);
    
    SubscribeToEvent(b, E_DRAGMOVE, URHO3D_HANDLER(App, HandleDragMove));
    SubscribeToEvent(b, E_DRAGBEGIN, URHO3D_HANDLER(App, HandleDragBegin));
    SubscribeToEvent(b, E_DRAGCANCEL, URHO3D_HANDLER(App, HandleDragCancel));
    SubscribeToEvent(b, E_DRAGEND, URHO3D_HANDLER(App, HandleDragEnd));
  }
  
  
  void HandleDragBegin(StringHash eventType, VariantMap& eventData)
  {
    using namespace DragBegin;
    Button* element = (Button*)eventData[P_ELEMENT].GetVoidPtr();
    
    int lx = eventData[P_X].GetInt();
    int ly = eventData[P_Y].GetInt();
    
    IntVector2 p = element->GetPosition();
    element->SetVar("START", p);
    element->SetVar("DELTA", IntVector2(p.x_ - lx, p.y_ - ly));
    
    int buttons = eventData[P_BUTTONS].GetInt();
    element->SetVar("BUTTONS", buttons);
    
//    Text* t = (Text*)element->GetChild(String("Text"));
//    t->SetText("Drag Begin Buttons: " + String(buttons));
//    
//    t = (Text*)element->GetChild(String("Num Touch"));
//    t->SetText("Number of buttons: " + String(eventData[P_NUMBUTTONS].GetInt()));
  }
  
  void HandleDragMove(StringHash eventType, VariantMap& eventData)
  {
    using namespace DragBegin;
    Button* element = (Button*)eventData[P_ELEMENT].GetVoidPtr();
    int buttons = eventData[P_BUTTONS].GetInt();
    IntVector2 d = element->GetVar("DELTA").GetIntVector2();
    int X = eventData[P_X].GetInt() + d.x_;
    int Y = eventData[P_Y].GetInt() + d.y_;
    int BUTTONS = element->GetVar("BUTTONS").GetInt();
    
//    Text* t = (Text*)element->GetChild(String("Event Touch"));
//    t->SetText("Drag Move Buttons: " + String(buttons));
    
    if (buttons == BUTTONS)
      element->SetPosition(IntVector2(X, Y));
  }
  
  void HandleDragCancel(StringHash eventType, VariantMap& eventData)
  {
    using namespace DragBegin;
    Button* element = (Button*)eventData[P_ELEMENT].GetVoidPtr();
    IntVector2 P = element->GetVar("START").GetIntVector2();
    element->SetPosition(P);
  }
  
  void HandleDragEnd(StringHash eventType, VariantMap& eventData)
  {
    using namespace DragBegin;
    Button* element = (Button*)eventData[P_ELEMENT].GetVoidPtr();
  }

  
  virtual void Stop()
  {
    // Perform optional cleanup after main loop has terminated
  }
  void HandleKeyDown(StringHash eventType, VariantMap& eventData)
  {
    using namespace KeyDown;
    // Check for pressing ESC. Note the engine_ member variable for convenience access to the Engine object
    int key = eventData[P_KEY].GetInt();
    if (key == KEY_ESCAPE)
      engine_->Exit();
  }
};
URHO3D_DEFINE_APPLICATION_MAIN(App)
